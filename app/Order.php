<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Cart;
class Order extends Model
{
    //
    protected $table='orders';
    protected $fillable=['user_id','content','file_src','product_name','status_id'];
    public function belongsUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }
	

    public function showBonCommand()
    {
        return $this->lists('bon_command_id')->unique();
    }

    public function getByBonId($query)
    {
        return $this->where('bon_command_id','=',$query)->get();
    }

    public function orderStatus()
    {
        return $this->belongsTo(Status::class,'status_id');
    }

	public function saveOrder($product_id,$product_name,$price,Array $array)
	{
		Cart::add($product_id,$product_name,$price,$array);
		alert()->success('购买成功', 'Success!');
		return redirect()->back();
	}
}
