<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Papier extends Model
{
    //
    protected $table='papier_list';
    protected $fillable=['papier','order'];

    public function showAll()
    {
        return Papier::all();
    }

    public function showOne($id)
    {
        return Papier::find($id)->papier;
    }

	public static function getPapierByOrder($format,$proid)
	{
		$papierIds=self::getPapierIds($format,$proid);
		return  Papier::whereIn('id',$papierIds)->orderBy('order')->get();
	}
	public static function getPapierIds($format,$proid)
	{
		$tables = Pricetablelist::where('formats', 'like', '%' . $format . '%')
			->where('product_id', $proid)
			->get();
		$papiers = array();
		foreach ($tables as $table) {
			$papiers = array_merge(json_decode($table->papiers), $papiers);
		}
		return array_unique($papiers);
    }
}
