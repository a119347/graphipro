<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pelliculage extends Model
{
    //
    protected $table='pelliculage_list';
    protected $fillable=['pelliculage','order'];

    public function showAll()
    {
        return Pelliculage::all();
    }

	public  function getPelliculageByOrder($format,$proid,$papier,$imprimer)
	{
		 $pelliculageIds=$this->getPelliculageIds($format,$proid,$papier,$imprimer);
		 return Pelliculage::whereIn('id',$pelliculageIds)->orderBy('order')->get();
	}
	protected function getPelliculageIds($format,$proid,$papier,$imprimer)
	{
		$tables = Pricetablelist::where('formats', 'like', '%' . $format . '%')
			->where('product_id', $proid)
			->where('papiers', 'like', '%' . $papier . '%')
			->where('imprimers', $imprimer)
			->get();
		$pelliculages = array();
		foreach ($tables as $table) {
			$pelliculages = array_merge(json_decode($table->pelliculages), $pelliculages);
		}
		return  array_unique($pelliculages);
    }
}
