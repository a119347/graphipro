<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livraison extends Model
{
    //
    protected $fillable=['postcode','price','numbers','product_id'];
    public $timestamps=false;
	protected $postcodes=array(75,77,78,91,92,93,94,95);
	public function LvPrice($postcode,$ex,$product_id)
	{
		$code=substr($postcode,0,2);
		if (in_array($code,$this->postcodes))
		{
			$post=1;
		}else{
			$post=2;
		}
		$exs=Livraison::where('product_id',$product_id)->groupBy('numbers')->get()->lists('numbers')->toArray();
		for ($i=0;$i<count($exs);$i++){
			if ($ex<=min($exs)){
				 $ex=min($exs);
			}elseif ($ex>max($exs)){
				 $ex=max($exs);
			}else{
				if ($ex<=$exs[$i]){
					 $ex=$exs[$i];
				}
			}
		}
		return Livraison::where('postcode',$post)
							->where('numbers',$ex)
							->where('product_id',$product_id)->first()->price;
	}
}
