<?php

namespace App\Http\Controllers;

use App\Pelliculage;
use Illuminate\Http\Request;

use App\Http\Requests;

class PelliculageController extends Controller
{
    //
	public function index()
	{
		return view('admin.product_attribute_template.pelliculage_template');
	}
    public function show()
    {
        return Pelliculage::orderBy('order')->get();
    }

    public function store(Request $request)
    {
        Pelliculage::create($request->all());
        return redirect()->back();
    }

	public function destroy($id)
	{
		Pelliculage::destroy($id);
    }

	public function update($id,Request $request)
	{
		Pelliculage::find($id)->update([
			'pelliculage'=>$request->get('pelliculage'),
			'order'=>$request->get('order')
		]);
		return redirect()->back();
	}
}
