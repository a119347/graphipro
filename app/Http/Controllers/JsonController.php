<?php

namespace App\Http\Controllers;

use App\FinishTime;
use App\Format;
use App\Livraison;
use App\Papier;
use App\Pelliculage;
use App\Price;
use App\Pricetablelist;
use Illuminate\Http\Request;

use App\Http\Requests;

class JsonController extends Controller
{
	//
	public function getFormat(Request $request)
	{
		$tables = Pricetablelist::where('product_id', $request->get('proid'))->get();
		$formats = array();
		foreach ($tables as $table) {
			$formats = array_merge(json_decode($table->formats), $formats);
		}
		$formats = array_unique($formats);
		foreach ($formats as $format) {
			echo '<option>' . Format::find($format)->format . '</option>';
		}
	}

	public function getPapier(Request $request)
	{
		$format=$request->get('format');
		$proid=$request->get('proid');
		$papierByOrder=Papier::getPapierByOrder($format,$proid);
		foreach ($papierByOrder as $papier) {
			echo '<option value="' . $papier->id . '">' . $papier->papier . '</option>';
		}
	}

	public function getImprimer(Request $request)
	{

		$tables = Pricetablelist::where('formats', 'like', '%' . $request->get('format') . '%')
			->where('product_id', $request->get('proid'))
			->where('papiers', 'like', '%' . $request->get('papier') . '%')
			->get();
		$imprimers = array();
		foreach ($tables as $table) {
			array_push($imprimers, $table->imprimers);
		}
		$imprimers = array_unique($imprimers);
		foreach ($imprimers as $imprimer) {
			if ($imprimer == 1) {

				echo '<option value="' . $imprimer . '">' . 'Recto' . '</option>';
			} else {
				echo '<option value="' . $imprimer . '">' . 'Recto et verso' . '</option>';
			}
		}
	}

	public function getPelle(Request $request)
	{
		$format=$request->get('format');
		$proid=$request->get('proid');
		$papier=$request->get('papier');
		$imprimer=$request->get('imprimer');
		$pelliculage=new Pelliculage();
		$pelliculageByOrder=$pelliculage->getPelliculageByOrder($format,$proid,$papier,$imprimer);
		foreach ($pelliculageByOrder as $pelliculage) {
			echo '<option value="' . $pelliculage->id . '">' . $pelliculage->pelliculage . '</option>';
		}
	}

	public function getPrice(Request $request)
	{
		$tables = Pricetablelist::where('formats', 'like', '%' . $request->get('format') . '%')
			->where('product_id', '=', $request->get('proid'))
			->where('papiers', 'like', '%' . $request->get('papier') . '%')
			->where('imprimers', '=', $request->get('imprimer'))
			->where('pelliculages', 'like', '%' . $request->get('pelliculage') . '%')
			->first();

		$prices = Price::where('price_table_list_id', '=', $tables->id)->orderBy('count')->get();
		$times = FinishTime::where('price_table_list_id', $tables->id)->first();
		$array = array('prices' => $prices, 'times' => $times);
		return json_encode($array);
	}

}
