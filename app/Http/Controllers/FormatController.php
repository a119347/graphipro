<?php

namespace App\Http\Controllers;

use App\Format;
use Illuminate\Http\Request;

use App\Http\Requests;

class FormatController extends Controller
{
    //
	public function index()
	{
		return view('admin.product_attribute_template.formate_template');
	}
    public function show()
    {
        return Format::orderBy('order')->get();
    }

    public function store(Request $request)
    {
        Format::create($request->all());
        return redirect()->back();
    }

	public function destroy($id)
	{
		Format::destroy($id);
    }

	public function update($id,Request $request)
	{
		Format::find($id)->update([
			'format'=>$request->get('format'),
			'order'=>$request->get('order')
		]);
		if ($request->hasFile('img')) {
			Format::find($id)->update([
				'format'=>$request->get('format'),
				'img'=>$request->file('img')->getClientOriginalName()
			]);
			$request->file('img')->move(('storage/uploads'), $request->file('img')->getClientOriginalName());
		}
		return redirect()->back();
    }


}
