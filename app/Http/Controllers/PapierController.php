<?php

namespace App\Http\Controllers;

use App\Papier;
use Illuminate\Http\Request;

use App\Http\Requests;

class PapierController extends Controller
{
    //
	public function index()
	{
		return view('admin.product_attribute_template.papier_template');
	}

    public function store(Request $request)
    {
        Papier::create($request->all());
        return redirect()->back();
    }

    public function show()
    {
        return Papier::orderBy('order')->get()->toJson();
    }

    public function destroy(Request $request)
    {
         Papier::destroy($request->id);

    }

	public function update($id,Request $request)
	{
		Papier::find($id)->update([
			'papier'=>$request->get('papier'),
			'order'=>$request->get('order')
		]);
		return redirect()->back();
    }
}
