<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Format extends Model
{
    //
    protected $table='format_list';
    protected $fillable=['format','img','order'];

    public function showAll()
    {
        return Format::all();
    }

    public function showOne($id)
    {
        return Format::find($id);
    }

	public function getFormatByOrder($productid)
	{
		$formatIds=$this->getFormatIds($productid);
		return collect(DB::table('format_list')->whereIn('id',$formatIds)->orderBy('order')->get());
    }
    protected function getFormatIds($productid)
    {
	    $tables=Pricetablelist::where('product_id',$productid)->get();
	    $formatIds=array();
	    foreach ($tables as $table) {
		    $formatIds= array_merge(json_decode($table->formats),$formatIds);
	    }
	   return  collect(array_unique($formatIds));
    }
}
