@extends('admin.template.admin_template')
@section('content')
	{!! Form::open() !!}
	<div class="form-group">
		{{Form::label('','Pelliculage')}}
		{{Form::text('pelliculage','',array('class'=>'form-control','id'=>'','required'=>'required'))}}
	</div>
	<div class="form-group">
		{{Form::submit('Submit',array('class'=>'btn btn-default pull-right','formaction'=>'pelliculage/create','formmethod'=>'post'))}}
	</div>
	{!! Form::close() !!}
	<pelliculage-template></pelliculage-template>
	<template id="pelliculage-list">
		<table class="table table-hover">
			<tr>
				<td>Pelliculage</td>
				<td>排序</td>
				<td>操作</td>
			</tr>
			<tr v-for="pelliculage in pelliculages">
				<td>@{{pelliculage.pelliculage}}</td>
				<td>@{{ pelliculage.order }}</td>
				<td>
					<button class="btn btn-danger" @click="delPelliculage(pelliculage)">删除</button>
					<button type="button" class="btn btn-default" data-toggle="modal"
							data-target="#modal"
							data-id="@{{ pelliculage.id }}"
							data-order="@{{ pelliculage.order }}"
							data-pelliculage="@{{ pelliculage.pelliculage }}">
						编辑
					</button>				</td>
			</tr>
		</table>
		<div class="modal fade" tabindex="-1" role="dialog" id="modal" data-toggle="modal"
			 aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="/admin/pelliculage" method="post" role="form" enctype="multipart/form-data" id="form">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
										aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Pelliculage</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="">Pelliculage</label>
								<input type="text" class="form-control" name="pelliculage" id="pelliculage" placeholder="">
							</div>
							<div class="form-group">
								<label for="">编号</label>
								<input type="number" class="form-control" name="order" id="order" placeholder="">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary" id="submit">Save changes</button>
						</div>
					</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	</template>
	<script src="/js/pelliculage-template.js"></script>
	<script>
		new Vue({
			el:'body'
		});
		$('#modal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var pelliculage = button.data('pelliculage');
			var id = button.data('id');
			var order = button.data('order');
			var modal = $(this);
			modal.find('#pelliculage').val(pelliculage);
			modal.find('#order').val(order);
			$("#form").attr("action", "/admin/pelliculage/" + id);
		});
	</script>
@endsection