<style>
	.modalDialog {
		position: fixed;
		font-family: Arial, Helvetica, sans-serif;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: rgba(0, 0, 0, 0.8);
		z-index: 99999;
		display: none;
		-webkit-transition: opacity 400ms ease-in;
		-moz-transition: opacity 400ms ease-in;
		transition: opacity 400ms ease-in;
		pointer-events: auto;
		overflow-x: hidden;
		overflow-y: auto;
	}

	.modalDialog > div {
		width: 630px;
		position: relative;
		margin: 10% auto;
		padding: 5px 20px 13px 20px;
		border-radius: 10px;
		background: #fff;
		overflow: hidden;
	}

	.modal-open {
		overflow: hidden;
	}

	li.list-item {
		float: left;
		list-style: none;
		margin: 0px 5px;
		position: relative;
		box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .4);
	}

	li.list-item:hover {
		cursor: pointer;
	}

	li.list-item, img.list-item-img {
		width: 200px;
		height: 200px;
	}

	ul.list-group {
		width: 630px;
		padding: 0px;
		overflow: hidden;
	}

	div.list-item-text {
		width: 100%;
		position: absolute;
		background: rgba(255, 255, 255, 0.6);
		text-align: center;
		bottom: -20px;
		display: block;
	}

</style>
<div id="openModal" class="modalDialog">
	<div>
		@foreach($formatByOrder->chunk(3) as $items)
			<ul class="list-group">
				@foreach($items as $format)
					<li class="list-item"
						data-formatid="{{$format->id}}" data-formattext="{{$format->format}}">
						<img class="list-item-img" src="{{url('/storage/uploads/'.$format->img)}}"
							 data-toggle="modal"
							 alt="">
						<div class="list-item-text">
							{{$format->format}}
						</div>
					</li>
				@endforeach
			</ul>
		@endforeach
	</div>
</div>
<script>
	$('#change_format').click(function (e) {
		e.preventDefault();
		showModal();
	});
	$('.list-item')
			.hover(function (event) {
				event.preventDefault();
				$(this).children('div.list-item-text').animate({
					'bottom': '0px'
				}, 150);
			}, function (event) {
				event.preventDefault();
				$(this).children('div.list-item-text').animate({
					'bottom': '-20px'
				});
			})
			.on('click',function () {
				var formatid=this.dataset.formatid;
				var formattext=this.dataset.formattext;
				saveModal(formatid,formattext);
			});

	function showModal() {
		$('#openModal').show();
		$('body').addClass('modal-open');
	}

	function closeModal() {
		$('#openModal').hide();
		$('body').removeClass('modal-open');
	}

	function saveModal(formatid,formattext) {
		closeModal();
		$('#text_format').html(formattext);
		$('#format_id').val(formatid);
	}
</script>